import java.net.*;
import java.io.*;

public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter printWriter;
    private BufferedReader bufferedReader;

    /**
     * Konstruktor som skapar en ny server socket med parametern som port nummer.
     * @param port Denna paratmetern sätter vilken port servern ska lyssna på.
     */
    public Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Denna metod vänta på att en client ska ansluta genom metoden accept() som retunerar clientens socket. Genom denna socketen kan vi skapa våra PrintWriters och InputStreamReaders för kommunikation mellen server
     * socketen och kilent socketen.
     */
    private void awaitConnection() {
        try {
            clientSocket = serverSocket.accept();
            printWriter = new PrintWriter(clientSocket.getOutputStream(), true);
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            System.out.println("Client succesfully connected to the server");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Detta är ett simpelt protokoll som väntar på att klienten skickar ett meddelande och svarar med samma sträng förutom "Recived: " i början av strängen.
     */
    private void runProtocol() {
       try {
           String line;
           while (true) {
               line = bufferedReader.readLine();
               if(line != null) {
                   printWriter.println("Recived: " + line);
               }
           }
       } catch (Exception e) {
           System.out.println(e);
       }
    }

    /**
     * Main metoden skapar en en server med portnummer 6666 och anropar därefter våra privata hjälp metoder.
     * @param args
     */
    public static void main(String[] args) {
        Server server = new Server(6666);
        System.out.println("Server is up and running...");
        server.awaitConnection();
        server.runProtocol();
    }
}
