import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private Socket socket;
    protected BufferedReader bufferedReader;
    protected PrintWriter printWriter;

    /**
     * Konstruktorn skapar en ny socket som ansluts till servern. Genom att ansluta till serverns socket kan vi skicka och ta emot genom våra PrintWriters och InputStreamReaders.
     * @param ip Ip till servern
     * @param port portnummret som servern lyssnar på
     */
    public Client(String ip, int port) {
        try {
            socket = new Socket(ip, port);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream(), true);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
