import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application {
    private String line;

    @Override
    public void start(Stage stage) throws Exception {
        Client client = new Client("127.0.0.1", 6666);
        TextArea textArea = new TextArea();
        textArea.setPrefWidth(500);
        textArea.setPrefHeight(480);
        textArea.setEditable(false);
        TextField textField = new TextField();

        /**
         * När enter trycks ner läser man av det som står i textfältet och skriver det till textrutan innan det skickas vidare till servern. Man läser därefter av serverns socket efter svar och skriver även ut det
         * i textrutan.
         */
        textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                try {
                    if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                        line = textField.getText();
                        client.printWriter.println(line);
                        textArea.appendText(line + "\n");
                        line = client.bufferedReader.readLine();
                        textArea.appendText(line + "\n");
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });

        VBox vBox = new VBox(textArea, textField);
        stage.setScene(new Scene(vBox,500,500));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
